CREATE SCHEMA football;

CREATE TABLE football.competition
(
  competition_id BIGINT NOT NULL
    CONSTRAINT competition_pkey
    PRIMARY KEY,
  caption        VARCHAR(256),
  league         VARCHAR(32),
  year           INTEGER
);

CREATE UNIQUE INDEX competition_competition_id_uindex
  ON football.competition (competition_id);

CREATE INDEX competition_year_index
  ON football.competition (year);

CREATE TABLE football.team
(
  team_id         BIGINT NOT NULL
    CONSTRAINT team_pkey
    PRIMARY KEY,
  team_name       VARCHAR(256),
  team_code       VARCHAR(32),
  team_short_name VARCHAR(128)
);

CREATE UNIQUE INDEX team_team_id_uindex
  ON football.team (team_id);

CREATE TABLE football.fixture
(
  fixture_id              BIGINT NOT NULL
    CONSTRAINT fixture_pkey
    PRIMARY KEY,
  competition_id          BIGINT
    CONSTRAINT fixture_competition_fk
    REFERENCES football.competition
    ON DELETE CASCADE,
  home_team_id            BIGINT
    CONSTRAINT fixture_home_team_fk
    REFERENCES football.team
    ON DELETE CASCADE,
  away_team_id            BIGINT
    CONSTRAINT fixture_away_team_fk
    REFERENCES football.team
    ON DELETE CASCADE,
  fixture_date            TIMESTAMP,
  fixture_status          VARCHAR(10),
  goals_home_team         INTEGER,
  goals_away_team         INTEGER,
  is_penalty              BOOLEAN,
  penalty_goald_home_team INTEGER,
  penalty_goald_away_team INTEGER
);

CREATE UNIQUE INDEX fixture_fixture_id_uindex
  ON football.fixture (fixture_id);

CREATE INDEX fixture_competition_id_index
  ON football.fixture (competition_id);

CREATE INDEX fixture_home_team_id_away_team_id_index
  ON football.fixture (home_team_id, away_team_id);


CREATE TABLE football.REQUEST_LOG
(
  REQUEST_LOG_ID  BIGINT NOT NULL
    CONSTRAINT request_log_pkey
    PRIMARY KEY,
  REQUEST_DATE            TIMESTAMP,
  DATA_LOAD_TYPE       VARCHAR(16),
  DATA_EXPORT_TYPE       VARCHAR(16),
  COMPETITION_NAME VARCHAR(128),
  TEAM_NAME VARCHAR(128),
  YEAR_FROM INTEGER,
  YEAR_TO INTEGER,
  FILE_NAME VARCHAR(128),
  RESULT_COUNT INTEGER
);

CREATE SEQUENCE log_sequence START 1;
