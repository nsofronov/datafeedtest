/**
 * Пакет содержит логику работы для выполнения экспорта списов матчей в XML и CSV
 *
 * @author n.sofronov
 */
package ru.nsofronov.datafeedtest.export;