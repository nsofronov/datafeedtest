package ru.nsofronov.datafeedtest.export.service;

import com.opencsv.CSVWriter;
import org.springframework.stereotype.Service;
import ru.nsofronov.datafeedtest.database.model.Fixture;
import ru.nsofronov.datafeedtest.export.model.ExportFixture;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Сервис экпорта в формат CSV
 *
 * @author nsofronov
 */
@Service(value = "csvExportService")
public class CSVExportService extends AbstractExportService {

	/**
	 * Сериализация в формат CSV
	 *
	 * @param list - список объектов для сериализации
	 * @return сериализованные данные
	 */
	@Override
	protected String serialize(List<ExportFixture> list) {
		try {
			StringWriter writer = new StringWriter();
			CSVWriter csvWriter = new CSVWriter(writer, CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER,
					CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);

			List<String> headers = Stream
					.of("Турнир", "Дата проведения", "Хазяева", "Гости", "Статус", "Голы, забитые хозяевами",
							"Голы, забитые гостями").collect(Collectors.toList());
			csvWriter.writeNext(headers.toArray(new String[headers.size()]));

			Function<ExportFixture, String[]> fixtureFunction = f -> {
				String competitionName = f.getCompetitionName();
				String date = Optional.ofNullable(f.getDate())
						.map(d -> new SimpleDateFormat("dd-M-yyyy hh:mm:ss").format(d)).orElse(null);
				String homeTeam = f.getHomeTeam();
				String awayTeam = f.getAwayTeam();
				String status = f.getStatus();
				String goalsHomeTeam = Optional.ofNullable(f.getGoalsHomeTeam()).map(Object::toString).orElse(null);
				String goalsAwayTeam = Optional.ofNullable(f.getGoalsAwayTeam()).map(Object::toString).orElse(null);
				List<String> strings = Stream
						.of(competitionName, date, homeTeam, awayTeam, status, goalsHomeTeam, goalsAwayTeam)
						.collect(Collectors.toList());
				return strings.toArray(new String[strings.size()]);
			};
			csvWriter.writeAll(list.stream().map(fixtureFunction).collect(Collectors.toList()));
			csvWriter.close();
			writer.close();
			return writer.toString();
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}
}
