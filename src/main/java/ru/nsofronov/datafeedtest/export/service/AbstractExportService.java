package ru.nsofronov.datafeedtest.export.service;

import ru.nsofronov.datafeedtest.database.model.Fixture;
import ru.nsofronov.datafeedtest.export.model.ExportFixture;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Абстрактый сервис, содержащий необходимы для экспорта методы,
 * другие сервисы для экспорта могут наследоваться от этого класса
 *
 * @author nsofronov
 */
public abstract class AbstractExportService implements ExportService {

	/**
	 * Определяет специфическую сериализацию объектов для каждого из видов экпортов
	 * Должен быть реализован во всех потомках
	 *
	 * @param list - список объектов для сериализации
	 * @return
	 */
	protected abstract String serialize(List<ExportFixture> list);

	/**
	 * Конвертирет полученные фалй в удобный для сериализации вид, выполняет сериализацию
	 * и записывает полученный результат в файл для импорта
	 *
	 * @param fixtures список объектов для экспорта
	 * @return
	 */
	@Override
	public InputStream exportFixtures(List<Fixture> fixtures) {
		List<ExportFixture> list = fixtures.stream().map(this::prepareToExport).collect(Collectors.toList());
		return getStreamedContent(serialize(list));
	}

	/**
	 * Записывает сериализованные данные в файл и формирует из него
	 *
	 * @param export сериализованные данные, которые требуется записать в файл
	 * @return записанный файл экпорта
	 */
	private InputStream getStreamedContent(String export) {
		try {
			File file = File.createTempFile("AbstractExportService.export", ".txt");
			try (FileOutputStream out = new FileOutputStream(file)) {
				out.write(export.getBytes("UTF-8"));
			}
			return new FileInputStream(file);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Конвертирование
	 * @param f
	 * @return
	 */
	private ExportFixture prepareToExport(Fixture f) {
		ExportFixture fixture = new ExportFixture();
		fixture.setCompetitionName(f.getCompetition().getCaption());
		fixture.setDate(f.getDate());
		fixture.setHomeTeam(f.getHomeTeam().getName());
		fixture.setAwayTeam(f.getAwayTeam().getName());
		fixture.setStatus(f.getStatus().getName());
		fixture.setGoalsHomeTeam(f.getGoalsHomeTeam());
		fixture.setGoalsAwayTeam(f.getGoalsAwayTeam());
		return fixture;
	}
}
