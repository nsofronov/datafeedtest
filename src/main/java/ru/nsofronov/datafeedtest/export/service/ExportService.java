package ru.nsofronov.datafeedtest.export.service;

import ru.nsofronov.datafeedtest.database.model.Fixture;

import java.io.InputStream;
import java.util.List;

/**
 * Интерфейс, описываюий экпорт в различные форматы
 *
 * @author nsofronov
 */
public interface ExportService {
	/**
	 * Формирование файла экпорта
	 *
	 * @param list - список объектов для сериализации
	 * @return файл ипорта
	 */
	public InputStream exportFixtures(List<Fixture> list);
}
