package ru.nsofronov.datafeedtest.export.service;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.springframework.stereotype.Service;
import ru.nsofronov.datafeedtest.export.model.ExportFixture;

import java.util.List;

/**
 * Сервис экпорта в формат XML
 *
 * @author nsofronov
 */
@Service(value = "xmlExportService")
public class XMLExportService extends AbstractExportService {

	/**
	 * Сериализация в формат XML
	 *
	 * @param list - список объектов для сериализации
	 * @return сериализованные данные
	 */
	@Override
	protected String serialize(List<ExportFixture> list) {
		XStream xStream = new XStream(new DomDriver());
		xStream.alias("Match", ExportFixture.class);
		xStream.alias("Matches", List.class);
		return "<?xml version=\"1.0\"?>\n" + xStream.toXML(list);
	}
}
