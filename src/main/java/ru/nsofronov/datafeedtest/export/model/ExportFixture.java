package ru.nsofronov.datafeedtest.export.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

/**
 * Сущность для выполнения экпорта
 *
 * @author n.sofronov
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Fixture")
public class ExportFixture implements Serializable {
	@XmlElement(name = "CompetitionName")
	private String competitionName;
	@XmlElement(name = "Date")
	private Date date;
	@XmlElement(name = "HomeTeam")
	private String homeTeam;
	@XmlElement(name = "AwayTeam")
	private String awayTeam;
	@XmlElement(name = "Status")
	private String status;
	@XmlElement(name = "GoalsHomeTeam")
	private Integer goalsHomeTeam;
	@XmlElement(name = "GoalsAwayTeam")
	private Integer goalsAwayTeam;

	public String getCompetitionName() {
		return competitionName;
	}

	public void setCompetitionName(String competitionName) {
		this.competitionName = competitionName;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(String awayTeam) {
		this.awayTeam = awayTeam;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getGoalsHomeTeam() {
		return goalsHomeTeam;
	}

	public void setGoalsHomeTeam(Integer goalsHomeTeam) {
		this.goalsHomeTeam = goalsHomeTeam;
	}

	public Integer getGoalsAwayTeam() {
		return goalsAwayTeam;
	}

	public void setGoalsAwayTeam(Integer goalsAwayTeam) {
		this.goalsAwayTeam = goalsAwayTeam;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return date;
	}
}
