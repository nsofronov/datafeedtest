package ru.nsofronov.datafeedtest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.nsofronov.datafeedtest.api.DataExportType;
import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;
import ru.nsofronov.datafeedtest.api.DataLoadService;
import ru.nsofronov.datafeedtest.api.DataLoadType;
import ru.nsofronov.datafeedtest.database.service.LogService;
import ru.nsofronov.datafeedtest.export.service.ExportService;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Выпоняет сбор информации из внешгней системы, запись её в базу и формирование файла экспорта
 *
 * @author nsofronov
 */
public class Starter {
	private static final Logger logger = LogManager.getLogger(Starter.class);

	/**
	 * Запуск программы
	 *
	 * @param args аргументы программы
	 */
	public static void main(String[] args) {
		DataLoadRequest request = createRequest(args);
		if (request == null) {
			System.out.println("Запрос невалиден\nПожалуйста, измените параметры запроса и повторите его заново\n"
					+ "Корректный вид:\n");
			System.err.println("java -jar datafeedtest-1.0-shaded.jar Тип_запросов_для_получения_данных "
					+ "Тип_экпортируемого_значения Имя_команды Турнир Год_от/Год_до\n");
			System.out.println("Тип запросов для получения данных: " + Stream.of(DataLoadType.values()).map(Enum::toString)
					.reduce((s, s2) -> s + ", " + s2).orElse("нет значений"));
			System.out.println("Типы экпортируемых значений: " + Stream.of(DataExportType.values()).map(Enum::toString)
					.reduce((s, s2) -> s + ", " + s2).orElse("нет значений"));
			return;
		} else {
			System.out.println("По полученным параметрам выполняется формирование файла импорта\n"
					+ "Пожалуйста подождите...");
		}

		ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
		DataLoadService integrationService = context
				.getBean(request.getLoadType().toString().toLowerCase() + "DataLoadService", DataLoadService.class);

		DataLoadResponse response = loadData(request, integrationService);
		if (response == null)
			return;

		context.getBean(LogService.class).log(request, response);

		exportFile(context, request, response);
	}

	private static DataLoadResponse loadData(DataLoadRequest request, DataLoadService integrationService) {
		DataLoadResponse response = integrationService.load(request);
		if (response.getFixtures().isEmpty()) {
			logger.info("Ничего не было найдено");
			return null;
		}
		return response;
	}

	/**
	 * Формирование файла экспорта
	 *
	 * @param context  Spring context
	 * @param request  запрос данных
	 * @param response ответ, полученных из загрузчиков данных и содержащий файлы для импорта
	 */
	private static void exportFile(ApplicationContext context, DataLoadRequest request, DataLoadResponse response) {
		DataExportType exportType = request.getExportType();
		Path newPath = Paths.get(Paths.get("").toAbsolutePath().toString(), response.getName(request));
		ExportService bean = context.getBean(exportType.name().toLowerCase() + "ExportService", ExportService.class);
		try (InputStream inputStream = bean.exportFixtures(response.getFixtures())) {
			System.out.println("Будет создан файл " + newPath.toString());
			if (Files.exists(newPath))
				Files.delete(newPath);
			Files.copy(inputStream, newPath);
			inputStream.close();
		} catch (Throwable e) {
			System.err.println("Невозможно создать файл " + newPath.toString() + " из-за ошибки " + e.getMessage());
		}
	}

	/**
	 * Валидация входных параметров и формирование запроса, которым будет пользоваться система внутри себя
	 *
	 * @param args - аргументы запуска программы
	 * @return запрос данных для использования в других частях программы
	 */
	private static DataLoadRequest createRequest(String[] args) {
		DataLoadRequest request = new DataLoadRequest();
		if (args.length != 5) {
			System.err.println("Неверное число параметров");
			return null;
		}

		request.setLoadType(DataLoadType.getType(args[0]));
		if (request.getLoadType() == null) {
			System.err.println("Неправильно указан тип загрузки информации");
			return null;
		}

		request.setExportType(DataExportType.getType(args[1]));
		if (request.getExportType() == null) {
			System.err.println("Неправильно указан тип экпортируемого значения");
			return null;
		}

		request.setTeamName(args[2]);
		request.setCompetitionName(args[3]);

		String years = args[4];
		if (!years.matches("^\\d\\d\\d\\d/\\d\\d\\d\\d$")) {
			System.err.println("Некорректный формат даты " + years);
			return null;
		}

		request.setYearFrom(Integer.parseInt(years.substring(0, 4)) - 1);
		request.setYearTo(Integer.parseInt(years.substring(0, 4)));

		if (request.getYearFrom() > request.getYearTo()) {
			System.err.println("Некорректный формат даты " + years);
			return null;
		}

		return request;
	}
}
