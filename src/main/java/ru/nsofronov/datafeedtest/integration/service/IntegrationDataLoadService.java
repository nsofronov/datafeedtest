package ru.nsofronov.datafeedtest.integration.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;
import ru.nsofronov.datafeedtest.api.DataLoadService;
import ru.nsofronov.datafeedtest.integration.model.CompetitionFixture;
import ru.nsofronov.datafeedtest.integration.model.CompetitionTeams;
import ru.nsofronov.datafeedtest.integration.model.IntegrationCompetition;
import ru.nsofronov.datafeedtest.integration.model.IntegrationFixture;
import ru.nsofronov.datafeedtest.integration.model.IntegrationTeam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Сервис загрузки данных из внешней системы http://api.football-data.org
 * Загружает данные и конвертирует их во внутренний формат
 *
 * @author nsofronov
 */
@Service("integrationDataLoadService")
public class IntegrationDataLoadService implements DataLoadService {
	private static final Logger logger = LogManager.getLogger(IntegrationDataLoadService.class);
	private static String FIXTURES_URL = "http://api.football-data.org/v1/competitions/%s/fixtures";
	private static String TEAMS_URL = "http://api.football-data.org/v1/competitions/%s/teams";
	private static String COMPETITIONS_URL = "http://api.football-data.org/v1/competitions/?season=%s";

	@Autowired
	private ConversionService сonversionService;

	@Autowired
	@Qualifier("footballDataAuthToken")
	private String authToken;

	/**
	 * Загружает данные и конвертирует их во внутренний формат
	 *
	 * @param request запрос данных
	 * @return ответ, содержащий в себе
	 */
	@Override
	public DataLoadResponse load(DataLoadRequest request) {
		DataLoadResponse response = new DataLoadResponse();

		Stream<Integer> yearStream = Stream.iterate(request.getYearFrom(), i -> ++i)
				.limit(request.getYearTo() - request.getYearFrom() + 1);

		Predicate<IntegrationCompetition> competitionFilter = c -> request.getCompetitionName() == null || c.getLeague()
				.equals(request.getCompetitionName()) || c.getCaption().equals(request.getCompetitionName());
		List<IntegrationCompetition> competitions = yearStream.map(this::readCompetitions).flatMap(c -> c)
				.filter(competitionFilter).collect(Collectors.toList());
		if (competitions.isEmpty())
			return response;
		response.setCompetitions(сonversionService.toDataBaseCompetitions(competitions));

		Predicate<IntegrationTeam> teamFilter = t -> t.getCode().equals(request.getTeamName()) || t.getName()
				.equals(request.getTeamName()) || t.getShortName().equals(request.getTeamName());
		List<IntegrationTeam> allTeams = competitions.stream().map(this::readTeams).flatMap(t -> t).distinct()
				.collect(Collectors.toList());
		IntegrationTeam team = allTeams.stream().filter(teamFilter).findFirst().orElse(null);
		if (team == null)
			return response;
		response.setTeam(сonversionService.toDataBaseTeam(team));

		Predicate<IntegrationFixture> fixtureFilter = f -> team.getId().equals(f.getHomeTeamId()) || team.getId()
				.equals(f.getAwayTeamId());
		List<IntegrationFixture> fixtures = competitions.stream().map(this::readResults).flatMap(r -> r)
				.filter(fixtureFilter).collect(Collectors.toList());
		response.setFixtures(сonversionService.toDataBaseFixtures(fixtures, competitions, allTeams));

		return response;
	}

	/**
	 * Получение рузультатов матчей из внешней системы
	 *
	 * @param competition - турнир, в рамках которого нужно получить все матчи
	 * @return список прошедших и планируемых матчей в указанном турнире
	 */
	protected Stream<IntegrationFixture> readResults(IntegrationCompetition competition) {
		String url = String.format(FIXTURES_URL, competition.getId().toString());
		CompetitionFixture competitionFixture = executeRequest(url, CompetitionFixture.class);
		return Optional.ofNullable(competitionFixture).map(f -> f.getFixtures().stream()).orElse(Stream.empty());
	}

	/**
	 * Получение списка команд, участвующий в турнире
	 *
	 * @param competition - турнир, в рамках которого нужно получить все матчи
	 * @return список команд
	 */
	protected Stream<IntegrationTeam> readTeams(IntegrationCompetition competition) {
		String url = String.format(TEAMS_URL, competition.getId().toString());
		CompetitionTeams competionTeams = executeRequest(url, CompetitionTeams.class);
		return Optional.ofNullable(competionTeams).map(t -> t.getTeams().stream()).orElse(Stream.empty());
	}

	/**
	 * Получение списка турниров, прошедших в определённый год
	 *
	 * @param year год, по которому ищем турниры
	 * @return список турниров
	 */
	protected Stream<IntegrationCompetition> readCompetitions(Integer year) {
		String url = String.format(COMPETITIONS_URL, Optional.ofNullable(year).orElse(2017).toString());
		IntegrationCompetition[] competitions = executeRequest(url, IntegrationCompetition[].class);
		return Optional.ofNullable(competitions).map(Stream::of).orElse(Stream.empty());
	}

	/**
	 * Общий метод вызова REST процедур
	 *
	 * @param url - ссылка, по которой нужно получить данные
	 * @param aClass - формат данных, к которому приводим json
	 * @param <T> - формат данных, к которому приводим json
	 * @return объект, полученный по интеграции
	 */
	protected <T> T executeRequest(String url, Class<T> aClass) {
		logger.debug(url);
		HttpURLConnection conn;
		try {
			conn = (HttpURLConnection) new URL(url).openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("X-Auth-Token", authToken);
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			T t = new ObjectMapper().readValue(br, aClass);
			conn.disconnect();
			return t;
		} catch (IOException e) {
			logger.error("При вызове " + url + " произошла ошибка " + e.getMessage());
			return null;
		}
	}
}
