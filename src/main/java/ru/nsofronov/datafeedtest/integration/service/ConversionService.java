package ru.nsofronov.datafeedtest.integration.service;

import org.springframework.stereotype.Component;
import ru.nsofronov.datafeedtest.database.model.Competition;
import ru.nsofronov.datafeedtest.database.model.Fixture;
import ru.nsofronov.datafeedtest.database.model.FixtureStatus;
import ru.nsofronov.datafeedtest.database.model.Team;
import ru.nsofronov.datafeedtest.integration.model.FixtureResultGoals;
import ru.nsofronov.datafeedtest.integration.model.IntegrationCompetition;
import ru.nsofronov.datafeedtest.integration.model.IntegrationFixture;
import ru.nsofronov.datafeedtest.integration.model.IntegrationTeam;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Компонент для конвертации данных, полученных по интеграции в сущности, готовые, для записи в базу
 *
 * @author nsofronov
 */
@Component
public class ConversionService implements Serializable {
	/**
	 * Конвертирование турниров
	 *
	 * @param competitions список турниров
	 * @return список турниров
	 */
	public List<Competition> toDataBaseCompetitions(List<IntegrationCompetition> competitions) {
		return competitions.stream().map(this::toDataBaseCompetition).collect(Collectors.toList());
	}

	/**
	 * Конвертирование турниров
	 *
	 * @param integrationCompetition турнир
	 * @return турнир
	 */
	private Competition toDataBaseCompetition(IntegrationCompetition integrationCompetition) {
		if (integrationCompetition == null)
			return null;
		Competition competition = new Competition(integrationCompetition.getId());

		competition.setCaption(integrationCompetition.getCaption());
		competition.setLeague(integrationCompetition.getLeague());
		competition.setYear(integrationCompetition.getYear());

		return competition;
	}

	/**
	 * Конвертирование команд
	 *
	 * @param teams команды
	 * @return команды
	 */
	public List<Team> toDataBaseTeams(List<IntegrationTeam> teams) {
		return teams.stream().map(this::toDataBaseTeam).collect(Collectors.toList());
	}

	/**
	 * Конвертирование команд
	 *
	 * @param integrationTeam команда
	 * @return команда
	 */
	public Team toDataBaseTeam(IntegrationTeam integrationTeam) {
		if (integrationTeam == null)
			return null;
		Team team = new Team(integrationTeam.getId());

		team.setCode(integrationTeam.getCode());
		team.setShortName(integrationTeam.getShortName());
		team.setName(integrationTeam.getName());

		return team;
	}

	/**
	 * Конвертирование матчей
	 *
	 * @param fixtures матчи
	 * @param competitions турниры
	 * @param allTeams команды
	 * @return матчи
	 */
	public List<Fixture> toDataBaseFixtures(List<IntegrationFixture> fixtures,
			List<IntegrationCompetition> competitions, List<IntegrationTeam> allTeams) {
		return fixtures.stream().map(f -> toDataBaseFixture(f, competitions, allTeams)).collect(Collectors.toList());
	}

	/**
	 * Конвертирование матчей
	 *
	 * @param integrationFixture матч
	 * @param competitions турниры
	 * @param allTeams команды
	 * @return матчи
	 */
	private Fixture toDataBaseFixture(IntegrationFixture integrationFixture, List<IntegrationCompetition> competitions,
			List<IntegrationTeam> allTeams) {
		if (integrationFixture == null)
			return null;
		Fixture fixture = new Fixture(integrationFixture.getId());

		IntegrationCompetition competition = competitions.stream()
				.filter(c -> c.getId().equals(integrationFixture.getCompetitionId())).findFirst().orElse(null);
		fixture.setCompetition(toDataBaseCompetition(competition));
		IntegrationTeam homeTeam = allTeams.stream().filter(t -> t.getId().equals(integrationFixture.getHomeTeamId()))
				.findFirst().orElse(null);
		fixture.setHomeTeam(toDataBaseTeam(homeTeam));
		IntegrationTeam awayTeam = allTeams.stream().filter(t -> t.getId().equals(integrationFixture.getAwayTeamId()))
				.findFirst().orElse(null);
		fixture.setAwayTeam(toDataBaseTeam(awayTeam));
		fixture.setDate(integrationFixture.getDate());
		fixture.setStatus(FixtureStatus.getStatus(integrationFixture.getStatus()));
		fixture.setGoalsHomeTeam(integrationFixture.getResult().getGoalsAwayTeam());
		fixture.setGoalsAwayTeam(integrationFixture.getResult().getGoalsAwayTeam());
		fixture.setPenalty(integrationFixture.getResult().getPenaltyShootout() != null);
		fixture.setPenaltyGoalsHomeTeam(Optional.ofNullable(integrationFixture.getResult().getPenaltyShootout())
				.map(FixtureResultGoals::getGoalsHomeTeam).orElse(0));
		fixture.setPenaltyGoalsAwayTeam(Optional.ofNullable(integrationFixture.getResult().getPenaltyShootout())
				.map(FixtureResultGoals::getGoalsAwayTeam).orElse(0));

		return fixture;
	}
}
