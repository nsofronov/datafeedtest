/**
 * Пакет содержит логику работы с системой http://api.football-data.org/ с ипользованием Rest
 * В пакете model содержится набор сущностей, которые мы можем получить из внешней системы
 *
 * @author n.sofronov
 */
package ru.nsofronov.datafeedtest.integration;