package ru.nsofronov.datafeedtest.integration.model;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * @author nsofronov
 */
public class IntegrationFixture implements Serializable {
	private static final String URL_PATTERN = "http://api.football-data.org/v1/fixtures/";
	private FixtureLinks _links;
	private Date date;
	private String status;
	private Integer matchday;
	private String homeTeamName;
	private String awayTeamName;
	private FixtureResult result;
	private FixtureOdds odds;

	@Transient
	public Long getId() {
		return Long.parseLong(get_links().getSelf().getHref().substring(URL_PATTERN.length()));
	}

	@Transient
	public Long getCompetitionId() {
		return Long.parseLong(get_links().getCompetition().getHref().substring(IntegrationCompetition.URL_PATTERN.length()));
	}

	@Transient
	public Long getHomeTeamId() {
		return Long.parseLong(get_links().getHomeTeam().getHref().substring(IntegrationTeam.URL_PATTERN.length()));
	}

	@Transient
	public Long getAwayTeamId() {
		return Long.parseLong(get_links().getAwayTeam().getHref().substring(IntegrationTeam.URL_PATTERN.length()));
	}

	public FixtureLinks get_links() {
		return _links;
	}

	public void set_links(FixtureLinks _links) {
		this._links = _links;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getMatchday() {
		return matchday;
	}

	public void setMatchday(Integer matchday) {
		this.matchday = matchday;
	}

	public String getHomeTeamName() {
		return homeTeamName;
	}

	public void setHomeTeamName(String homeTeamName) {
		this.homeTeamName = homeTeamName;
	}

	public String getAwayTeamName() {
		return awayTeamName;
	}

	public void setAwayTeamName(String awayTeamName) {
		this.awayTeamName = awayTeamName;
	}

	public FixtureResult getResult() {
		return result;
	}

	public void setResult(FixtureResult result) {
		this.result = result;
	}

	public FixtureOdds getOdds() {
		return odds;
	}

	public void setOdds(FixtureOdds odds) {
		this.odds = odds;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		IntegrationFixture fixture = (IntegrationFixture) o;

		if (_links != null ? !_links.equals(fixture._links) : fixture._links != null)
			return false;
		if (date != null ? !date.equals(fixture.date) : fixture.date != null)
			return false;
		if (status != null ? !status.equals(fixture.status) : fixture.status != null)
			return false;
		if (matchday != null ? !matchday.equals(fixture.matchday) : fixture.matchday != null)
			return false;
		if (homeTeamName != null ? !homeTeamName.equals(fixture.homeTeamName) : fixture.homeTeamName != null)
			return false;
		if (awayTeamName != null ? !awayTeamName.equals(fixture.awayTeamName) : fixture.awayTeamName != null)
			return false;
		if (result != null ? !result.equals(fixture.result) : fixture.result != null)
			return false;
		return odds != null ? odds.equals(fixture.odds) : fixture.odds == null;
	}

	@Override
	public int hashCode() {
		int result1 = _links != null ? _links.hashCode() : 0;
		result1 = 31 * result1 + (date != null ? date.hashCode() : 0);
		result1 = 31 * result1 + (status != null ? status.hashCode() : 0);
		result1 = 31 * result1 + (matchday != null ? matchday.hashCode() : 0);
		result1 = 31 * result1 + (homeTeamName != null ? homeTeamName.hashCode() : 0);
		result1 = 31 * result1 + (awayTeamName != null ? awayTeamName.hashCode() : 0);
		result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
		result1 = 31 * result1 + (odds != null ? odds.hashCode() : 0);
		return result1;
	}
}
