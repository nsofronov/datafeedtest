package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;

/**
 * @author nsofronov
 */
public class FixtureResult implements Serializable {
	private Integer goalsHomeTeam;
	private Integer goalsAwayTeam;
	private FixtureResultGoals halfTime;
	private FixtureResultGoals extraTime;
	private FixtureResultGoals penaltyShootout;

	public Integer getGoalsHomeTeam() {
		return goalsHomeTeam;
	}

	public void setGoalsHomeTeam(Integer goalsHomeTeam) {
		this.goalsHomeTeam = goalsHomeTeam;
	}

	public Integer getGoalsAwayTeam() {
		return goalsAwayTeam;
	}

	public void setGoalsAwayTeam(Integer goalsAwayTeam) {
		this.goalsAwayTeam = goalsAwayTeam;
	}

	public FixtureResultGoals getHalfTime() {
		return halfTime;
	}

	public void setHalfTime(FixtureResultGoals halfTime) {
		this.halfTime = halfTime;
	}

	public FixtureResultGoals getExtraTime() {
		return extraTime;
	}

	public void setExtraTime(FixtureResultGoals extraTime) {
		this.extraTime = extraTime;
	}

	public FixtureResultGoals getPenaltyShootout() {
		return penaltyShootout;
	}

	public void setPenaltyShootout(FixtureResultGoals penaltyShootout) {
		this.penaltyShootout = penaltyShootout;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		FixtureResult that = (FixtureResult) o;

		if (goalsHomeTeam != null ? !goalsHomeTeam.equals(that.goalsHomeTeam) : that.goalsHomeTeam != null)
			return false;
		if (goalsAwayTeam != null ? !goalsAwayTeam.equals(that.goalsAwayTeam) : that.goalsAwayTeam != null)
			return false;
		if (halfTime != null ? !halfTime.equals(that.halfTime) : that.halfTime != null)
			return false;
		if (extraTime != null ? !extraTime.equals(that.extraTime) : that.extraTime != null)
			return false;
		return penaltyShootout != null ? penaltyShootout.equals(that.penaltyShootout) : that.penaltyShootout == null;
	}

	@Override
	public int hashCode() {
		int result = goalsHomeTeam != null ? goalsHomeTeam.hashCode() : 0;
		result = 31 * result + (goalsAwayTeam != null ? goalsAwayTeam.hashCode() : 0);
		result = 31 * result + (halfTime != null ? halfTime.hashCode() : 0);
		result = 31 * result + (extraTime != null ? extraTime.hashCode() : 0);
		result = 31 * result + (penaltyShootout != null ? penaltyShootout.hashCode() : 0);
		return result;
	}
}
