package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;

/**
 * @author nsofronov
 */
public class FixtureResultGoals implements Serializable {
	private Integer goalsHomeTeam;
	private Integer goalsAwayTeam;

	public Integer getGoalsHomeTeam() {
		return goalsHomeTeam;
	}

	public void setGoalsHomeTeam(Integer goalsHomeTeam) {
		this.goalsHomeTeam = goalsHomeTeam;
	}

	public Integer getGoalsAwayTeam() {
		return goalsAwayTeam;
	}

	public void setGoalsAwayTeam(Integer goalsAwayTeam) {
		this.goalsAwayTeam = goalsAwayTeam;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		FixtureResultGoals that = (FixtureResultGoals) o;

		if (goalsHomeTeam != null ? !goalsHomeTeam.equals(that.goalsHomeTeam) : that.goalsHomeTeam != null)
			return false;
		return goalsAwayTeam != null ? goalsAwayTeam.equals(that.goalsAwayTeam) : that.goalsAwayTeam == null;
	}

	@Override
	public int hashCode() {
		int result = goalsHomeTeam != null ? goalsHomeTeam.hashCode() : 0;
		result = 31 * result + (goalsAwayTeam != null ? goalsAwayTeam.hashCode() : 0);
		return result;
	}
}
