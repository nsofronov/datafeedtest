package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;

/**
 * @author nsofronov
 */
public class TeamLinks implements Serializable {
	private CompetitionLink self;
	private CompetitionLink fixtures;
	private CompetitionLink players;

	public CompetitionLink getSelf() {
		return self;
	}

	public void setSelf(CompetitionLink self) {
		this.self = self;
	}

	public CompetitionLink getFixtures() {
		return fixtures;
	}

	public void setFixtures(CompetitionLink fixtures) {
		this.fixtures = fixtures;
	}

	public CompetitionLink getPlayers() {
		return players;
	}

	public void setPlayers(CompetitionLink players) {
		this.players = players;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		TeamLinks teamLinks = (TeamLinks) o;

		if (self != null ? !self.equals(teamLinks.self) : teamLinks.self != null)
			return false;
		if (fixtures != null ? !fixtures.equals(teamLinks.fixtures) : teamLinks.fixtures != null)
			return false;
		return players != null ? players.equals(teamLinks.players) : teamLinks.players == null;
	}

	@Override
	public int hashCode() {
		int result = self != null ? self.hashCode() : 0;
		result = 31 * result + (fixtures != null ? fixtures.hashCode() : 0);
		result = 31 * result + (players != null ? players.hashCode() : 0);
		return result;
	}
}
