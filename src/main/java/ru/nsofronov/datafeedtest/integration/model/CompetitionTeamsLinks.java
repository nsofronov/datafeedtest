package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;

/**
 * @author nsofronov
 */
public class CompetitionTeamsLinks implements Serializable {
	private CompetitionLink self;
	private CompetitionLink competition;

	public CompetitionLink getSelf() {
		return self;
	}

	public void setSelf(CompetitionLink self) {
		this.self = self;
	}

	public CompetitionLink getCompetition() {
		return competition;
	}

	public void setCompetition(CompetitionLink competition) {
		this.competition = competition;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CompetitionTeamsLinks that = (CompetitionTeamsLinks) o;

		if (self != null ? !self.equals(that.self) : that.self != null)
			return false;
		return competition != null ? competition.equals(that.competition) : that.competition == null;
	}

	@Override
	public int hashCode() {
		int result = self != null ? self.hashCode() : 0;
		result = 31 * result + (competition != null ? competition.hashCode() : 0);
		return result;
	}
}
