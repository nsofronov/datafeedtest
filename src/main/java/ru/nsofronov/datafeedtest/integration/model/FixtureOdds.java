package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;

/**
 * @author nsofronov
 */
public class FixtureOdds implements Serializable {
	private Float homeWin;
	private Float draw;
	private Float awayWin;

	public Float getHomeWin() {
		return homeWin;
	}

	public void setHomeWin(Float homeWin) {
		this.homeWin = homeWin;
	}

	public Float getDraw() {
		return draw;
	}

	public void setDraw(Float draw) {
		this.draw = draw;
	}

	public Float getAwayWin() {
		return awayWin;
	}

	public void setAwayWin(Float awayWin) {
		this.awayWin = awayWin;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		FixtureOdds that = (FixtureOdds) o;

		if (homeWin != null ? !homeWin.equals(that.homeWin) : that.homeWin != null)
			return false;
		if (draw != null ? !draw.equals(that.draw) : that.draw != null)
			return false;
		return awayWin != null ? awayWin.equals(that.awayWin) : that.awayWin == null;
	}

	@Override
	public int hashCode() {
		int result = homeWin != null ? homeWin.hashCode() : 0;
		result = 31 * result + (draw != null ? draw.hashCode() : 0);
		result = 31 * result + (awayWin != null ? awayWin.hashCode() : 0);
		return result;
	}
}
