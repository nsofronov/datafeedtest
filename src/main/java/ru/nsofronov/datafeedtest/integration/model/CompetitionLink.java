package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;

/**
 * @author nsofronov
 */
public class CompetitionLink implements Serializable {
	private String href;

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CompetitionLink that = (CompetitionLink) o;

		return href != null ? href.equals(that.href) : that.href == null;
	}

	@Override
	public int hashCode() {
		return href != null ? href.hashCode() : 0;
	}
}
