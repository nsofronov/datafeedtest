package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;

/**
 * @author nsofronov
 */
public class CompetitionLinks implements Serializable {
	private CompetitionLink self;
	private CompetitionLink teams;
	private CompetitionLink fixtures;
	private CompetitionLink leagueTable;

	public CompetitionLink getSelf() {
		return self;
	}

	public void setSelf(CompetitionLink self) {
		this.self = self;
	}

	public CompetitionLink getTeams() {
		return teams;
	}

	public void setTeams(CompetitionLink teams) {
		this.teams = teams;
	}

	public CompetitionLink getFixtures() {
		return fixtures;
	}

	public void setFixtures(CompetitionLink fixtures) {
		this.fixtures = fixtures;
	}

	public CompetitionLink getLeagueTable() {
		return leagueTable;
	}

	public void setLeagueTable(CompetitionLink leagueTable) {
		this.leagueTable = leagueTable;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CompetitionLinks that = (CompetitionLinks) o;

		if (self != null ? !self.equals(that.self) : that.self != null)
			return false;
		if (teams != null ? !teams.equals(that.teams) : that.teams != null)
			return false;
		if (fixtures != null ? !fixtures.equals(that.fixtures) : that.fixtures != null)
			return false;
		return leagueTable != null ? leagueTable.equals(that.leagueTable) : that.leagueTable == null;
	}

	@Override
	public int hashCode() {
		int result = self != null ? self.hashCode() : 0;
		result = 31 * result + (teams != null ? teams.hashCode() : 0);
		result = 31 * result + (fixtures != null ? fixtures.hashCode() : 0);
		result = 31 * result + (leagueTable != null ? leagueTable.hashCode() : 0);
		return result;
	}
}
