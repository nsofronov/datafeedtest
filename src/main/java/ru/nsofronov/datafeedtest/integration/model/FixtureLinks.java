package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;

/**
 * @author nsofronov
 */
public class FixtureLinks implements Serializable {
	private CompetitionLink self;
	private CompetitionLink competition;
	private CompetitionLink homeTeam;
	private CompetitionLink awayTeam;

	public CompetitionLink getSelf() {
		return self;
	}

	public void setSelf(CompetitionLink self) {
		this.self = self;
	}

	public CompetitionLink getCompetition() {
		return competition;
	}

	public void setCompetition(CompetitionLink competition) {
		this.competition = competition;
	}

	public CompetitionLink getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(CompetitionLink homeTeam) {
		this.homeTeam = homeTeam;
	}

	public CompetitionLink getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(CompetitionLink awayTeam) {
		this.awayTeam = awayTeam;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		FixtureLinks that = (FixtureLinks) o;

		if (self != null ? !self.equals(that.self) : that.self != null)
			return false;
		if (competition != null ? !competition.equals(that.competition) : that.competition != null)
			return false;
		if (homeTeam != null ? !homeTeam.equals(that.homeTeam) : that.homeTeam != null)
			return false;
		return awayTeam != null ? awayTeam.equals(that.awayTeam) : that.awayTeam == null;
	}

	@Override
	public int hashCode() {
		int result = self != null ? self.hashCode() : 0;
		result = 31 * result + (competition != null ? competition.hashCode() : 0);
		result = 31 * result + (homeTeam != null ? homeTeam.hashCode() : 0);
		result = 31 * result + (awayTeam != null ? awayTeam.hashCode() : 0);
		return result;
	}
}
