package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;

/**
 * @author nsofronov
 */
public class IntegrationTeam implements Serializable {
	static final String URL_PATTERN = "http://api.football-data.org/v1/teams/";
	private TeamLinks _links;
	private String name;
	private String code;
	private String shortName;
	private String squadMarketValue;
	private String crestUrl;

	public Long getId() {
		return Long.parseLong(get_links().getSelf().getHref().substring(URL_PATTERN.length()));
	}

	public TeamLinks get_links() {
		return _links;
	}

	public void set_links(TeamLinks _links) {
		this._links = _links;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getSquadMarketValue() {
		return squadMarketValue;
	}

	public void setSquadMarketValue(String squadMarketValue) {
		this.squadMarketValue = squadMarketValue;
	}

	public String getCrestUrl() {
		return crestUrl;
	}

	public void setCrestUrl(String crestUrl) {
		this.crestUrl = crestUrl;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		IntegrationTeam team = (IntegrationTeam) o;

		if (_links != null ? !_links.equals(team._links) : team._links != null)
			return false;
		if (name != null ? !name.equals(team.name) : team.name != null)
			return false;
		if (code != null ? !code.equals(team.code) : team.code != null)
			return false;
		if (shortName != null ? !shortName.equals(team.shortName) : team.shortName != null)
			return false;
		if (squadMarketValue != null ? !squadMarketValue.equals(team.squadMarketValue) : team.squadMarketValue != null)
			return false;
		return crestUrl != null ? crestUrl.equals(team.crestUrl) : team.crestUrl == null;
	}

	@Override
	public int hashCode() {
		int result = _links != null ? _links.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (code != null ? code.hashCode() : 0);
		result = 31 * result + (shortName != null ? shortName.hashCode() : 0);
		result = 31 * result + (squadMarketValue != null ? squadMarketValue.hashCode() : 0);
		result = 31 * result + (crestUrl != null ? crestUrl.hashCode() : 0);
		return result;
	}
}
