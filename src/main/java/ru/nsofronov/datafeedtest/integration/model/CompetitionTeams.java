package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author nsofronov
 */
public class CompetitionTeams implements Serializable {
	private CompetitionTeamsLinks _links;
	private Integer count;
	private List<IntegrationTeam> teams;

	public CompetitionTeamsLinks get_links() {
		return _links;
	}

	public void set_links(CompetitionTeamsLinks _links) {
		this._links = _links;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<IntegrationTeam> getTeams() {
		return teams;
	}

	public void setTeams(List<IntegrationTeam> teams) {
		this.teams = teams;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CompetitionTeams that = (CompetitionTeams) o;

		if (_links != null ? !_links.equals(that._links) : that._links != null)
			return false;
		if (count != null ? !count.equals(that.count) : that.count != null)
			return false;
		return teams != null ? teams.equals(that.teams) : that.teams == null;
	}

	@Override
	public int hashCode() {
		int result = _links != null ? _links.hashCode() : 0;
		result = 31 * result + (count != null ? count.hashCode() : 0);
		result = 31 * result + (teams != null ? teams.hashCode() : 0);
		return result;
	}
}
