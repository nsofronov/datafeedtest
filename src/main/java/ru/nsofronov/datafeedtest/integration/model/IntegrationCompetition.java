package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nsofronov
 */
public class IntegrationCompetition implements Serializable {
	protected static final String URL_PATTERN = "http://api.football-data.org/v1/competitions/";
	private CompetitionLinks _links;
	private Long id;
	private String caption;
	private String league;
	private Integer year;
	private Integer currentMatchday;
	private Integer numberOfMatchdays;
	private Integer numberOfTeams;
	private Integer numberOfGames;
	private Date lastUpdated;

	public String getFullName() {
		return getLeague() + " " + getCaption();
	}

	public CompetitionLinks get_links() {
		return _links;
	}

	public void set_links(CompetitionLinks _links) {
		this._links = _links;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getCurrentMatchday() {
		return currentMatchday;
	}

	public void setCurrentMatchday(Integer currentMatchday) {
		this.currentMatchday = currentMatchday;
	}

	public Integer getNumberOfMatchdays() {
		return numberOfMatchdays;
	}

	public void setNumberOfMatchdays(Integer numberOfMatchdays) {
		this.numberOfMatchdays = numberOfMatchdays;
	}

	public Integer getNumberOfTeams() {
		return numberOfTeams;
	}

	public void setNumberOfTeams(Integer numberOfTeams) {
		this.numberOfTeams = numberOfTeams;
	}

	public Integer getNumberOfGames() {
		return numberOfGames;
	}

	public void setNumberOfGames(Integer numberOfGames) {
		this.numberOfGames = numberOfGames;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		IntegrationCompetition that = (IntegrationCompetition) o;

		if (_links != null ? !_links.equals(that._links) : that._links != null)
			return false;
		if (id != null ? !id.equals(that.id) : that.id != null)
			return false;
		if (caption != null ? !caption.equals(that.caption) : that.caption != null)
			return false;
		if (league != null ? !league.equals(that.league) : that.league != null)
			return false;
		if (year != null ? !year.equals(that.year) : that.year != null)
			return false;
		if (currentMatchday != null ? !currentMatchday.equals(that.currentMatchday) : that.currentMatchday != null)
			return false;
		if (numberOfMatchdays != null ?
				!numberOfMatchdays.equals(that.numberOfMatchdays) :
				that.numberOfMatchdays != null)
			return false;
		if (numberOfTeams != null ? !numberOfTeams.equals(that.numberOfTeams) : that.numberOfTeams != null)
			return false;
		if (numberOfGames != null ? !numberOfGames.equals(that.numberOfGames) : that.numberOfGames != null)
			return false;
		return lastUpdated != null ? lastUpdated.equals(that.lastUpdated) : that.lastUpdated == null;
	}

	@Override
	public int hashCode() {
		int result = _links != null ? _links.hashCode() : 0;
		result = 31 * result + (id != null ? id.hashCode() : 0);
		result = 31 * result + (caption != null ? caption.hashCode() : 0);
		result = 31 * result + (league != null ? league.hashCode() : 0);
		result = 31 * result + (year != null ? year.hashCode() : 0);
		result = 31 * result + (currentMatchday != null ? currentMatchday.hashCode() : 0);
		result = 31 * result + (numberOfMatchdays != null ? numberOfMatchdays.hashCode() : 0);
		result = 31 * result + (numberOfTeams != null ? numberOfTeams.hashCode() : 0);
		result = 31 * result + (numberOfGames != null ? numberOfGames.hashCode() : 0);
		result = 31 * result + (lastUpdated != null ? lastUpdated.hashCode() : 0);
		return result;
	}
}
