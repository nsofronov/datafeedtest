package ru.nsofronov.datafeedtest.integration.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author nsofronov
 */
public class CompetitionFixture implements Serializable {
	private CompetitionFixtureLinks _links;
	private Integer count;
	private List<IntegrationFixture> fixtures;

	public CompetitionFixtureLinks get_links() {
		return _links;
	}

	public void set_links(CompetitionFixtureLinks _links) {
		this._links = _links;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<IntegrationFixture> getFixtures() {
		return fixtures;
	}

	public void setFixtures(List<IntegrationFixture> fixtures) {
		this.fixtures = fixtures;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CompetitionFixture that = (CompetitionFixture) o;

		if (_links != null ? !_links.equals(that._links) : that._links != null)
			return false;
		if (count != null ? !count.equals(that.count) : that.count != null)
			return false;
		return fixtures != null ? fixtures.equals(that.fixtures) : that.fixtures == null;
	}

	@Override
	public int hashCode() {
		int result = _links != null ? _links.hashCode() : 0;
		result = 31 * result + (count != null ? count.hashCode() : 0);
		result = 31 * result + (fixtures != null ? fixtures.hashCode() : 0);
		return result;
	}
}
