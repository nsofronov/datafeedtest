package ru.nsofronov.datafeedtest.api;

/**
 * Запрос в систему для получения данных из различных источников и экпорта этих данных в различные форматы
 *
 * @author nsofronov
 */
public class DataLoadRequest {
	private DataLoadType loadType;
	private DataExportType exportType;
	private Integer yearFrom;
	private Integer yearTo;
	private String competitionName;
	private String teamName;

	public DataLoadType getLoadType() {
		return loadType;
	}

	public void setLoadType(DataLoadType loadType) {
		this.loadType = loadType;
	}

	public DataExportType getExportType() {
		return exportType;
	}

	public void setExportType(DataExportType exportType) {
		this.exportType = exportType;
	}

	public Integer getYearFrom() {
		return yearFrom;
	}

	public void setYearFrom(Integer yearFrom) {
		this.yearFrom = yearFrom;
	}

	public Integer getYearTo() {
		return yearTo;
	}

	public void setYearTo(Integer yearTo) {
		this.yearTo = yearTo;
	}

	public String getCompetitionName() {
		return competitionName;
	}

	public void setCompetitionName(String competitionName) {
		this.competitionName = competitionName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getName() {
		return getTeamName() + getCompetitionName() + getYearFrom() + getYearTo();
	}
}
