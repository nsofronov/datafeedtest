package ru.nsofronov.datafeedtest.api;

import java.util.stream.Stream;

/**
 * Тип загрузки данных для экспорта
 *
 * @author nsofronov
 */
public enum DataLoadType {
	FULL, DATABASE, INTEGRATION;

	public static DataLoadType getType(String arg) {
		return Stream.of(values()).filter(v -> v.toString().toLowerCase().equals(arg.toLowerCase())).findFirst()
				.orElse(null);
	}
}
