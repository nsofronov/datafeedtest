package ru.nsofronov.datafeedtest.api;

import java.util.stream.Stream;

/**
 * Тип экпортируемого файла
 *
 * @author nsofronov
 */
public enum DataExportType {
	XML(".xml", "text/xml"),
	CSV(".csv", "text/csv");

	private String extension;
	private String mimeType;

	DataExportType(String extension, String mimeType) {
		this.extension = extension;
		this.mimeType = mimeType;
	}

	public String getExtension() {
		return extension;
	}

	public String getMimeType() {
		return mimeType;
	}

	public static DataExportType getType(String arg) {
		return Stream.of(values()).filter(v -> v.toString().toLowerCase().equals(arg.toLowerCase())).findFirst()
				.orElse(null);
	}
}
