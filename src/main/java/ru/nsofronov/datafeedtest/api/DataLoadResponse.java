package ru.nsofronov.datafeedtest.api;

import ru.nsofronov.datafeedtest.database.model.Competition;
import ru.nsofronov.datafeedtest.database.model.Fixture;
import ru.nsofronov.datafeedtest.database.model.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Сформированный набор данных, полученных по запросу и подготовленных к экпорту
 *
 * @author nsofronov
 */
public class DataLoadResponse {
	private Team team;
	private List<Competition> competitions = new ArrayList<>();
	private List<Fixture> fixtures = new ArrayList<>();

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public List<Competition> getCompetitions() {
		return competitions;
	}

	public void setCompetitions(List<Competition> competitions) {
		this.competitions = competitions;
	}

	public List<Fixture> getFixtures() {
		return fixtures;
	}

	public void setFixtures(List<Fixture> fixtures) {
		this.fixtures = fixtures;
	}

	/**
	 * Формирование имени файла по хранимым данным
	 *
	 * @param request - запрос
	 * @return имя для файла экпорта
	 */
	public String getName(DataLoadRequest request) {
		List<String> collect = competitions.stream().map(Competition::getLeague).distinct()
				.collect(Collectors.toList());
		String league = collect.size() == 1 ? collect.get(0) : request.getCompetitionName();
		String teamName = team != null ? team.getShortName() : request.getTeamName();
		return teamName + league + request.getYearFrom() + request.getYearTo() + request.getExportType().getExtension();
	}
}
