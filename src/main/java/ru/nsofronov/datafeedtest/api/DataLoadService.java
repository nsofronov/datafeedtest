package ru.nsofronov.datafeedtest.api;

/**
 * Интерфейс загрущиков данных для экпорта
 *
 * @author nsofronov
 */
public interface DataLoadService {
	/**
	 * Загрузка данных из различных источников
	 *
	 * @param request запрос
	 * @return ответ
	 */
	public DataLoadResponse load(DataLoadRequest request);
}
