package ru.nsofronov.datafeedtest.database.service;

import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;
import ru.nsofronov.datafeedtest.database.model.Fixture;

import java.util.List;

/**
 * Репозиторий для работы с базой данных
 *
 * @author nsofronov
 */
public interface DatabaseDAO {

	/**
	 * Запрос матчей, хранящихся в базе
	 *
	 * @param yearFrom год от
	 * @param yearTo год до
	 * @param competitionName название турнира
	 * @param teamName название команды
	 * @return матчи
	 */
	public List<Fixture> findFixtures(Integer yearFrom, Integer yearTo, String competitionName, String teamName);

	/**
	 * Загрузка сущностей в базу
	 * Сперва проверяет, есть ли уже эта сущность была создана, то только обновит информацию
	 *
	 * @param response - набор загружаемых сущностей
	 */
	public void merge(DataLoadResponse response);

	/**
	 * Логирование пользовательских запросов
	 *
	 * @param request запрос
	 * @param response ответ
	 */
	void createLog(DataLoadRequest request, DataLoadResponse response);
}
