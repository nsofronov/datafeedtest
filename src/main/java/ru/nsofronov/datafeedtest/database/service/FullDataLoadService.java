package ru.nsofronov.datafeedtest.database.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;
import ru.nsofronov.datafeedtest.integration.service.IntegrationDataLoadService;

/**
 * Сервис, выполняющий полный процесс загрузки данных
 * Загружает данные из внешней системы и переносит их в базу
 *
 * @author nsofronov
 */
@Service("fullDataLoadService")
public class FullDataLoadService extends IntegrationDataLoadService {
	@Autowired
	private DatabaseDAO databaseDAO;

	/**
	 * Загрузка данных
	 *
	 * @param request запрос данных
	 * @return результат загрузки
	 */
	@Override
	public DataLoadResponse load(DataLoadRequest request) {
		DataLoadResponse response = super.load(request);
		if (!response.getFixtures().isEmpty()) databaseDAO.merge(response);
		return response;
	}
}
