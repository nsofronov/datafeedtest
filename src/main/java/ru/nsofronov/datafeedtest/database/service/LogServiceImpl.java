package ru.nsofronov.datafeedtest.database.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;

/**
 * Сервис логирования запросов пользователя
 *
 * @author nsofronov
 */
@Service
public class LogServiceImpl implements LogService {

	@Autowired
	private DatabaseDAO databaseDAO;

	/**
	 * Выполнение логирования
	 *
	 * @param request запрос
	 * @param response ответ
	 */
	@Override
	public void log(DataLoadRequest request, DataLoadResponse response) {
		databaseDAO.createLog(request, response);
	}
}
