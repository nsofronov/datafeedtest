package ru.nsofronov.datafeedtest.database.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;
import ru.nsofronov.datafeedtest.api.DataLoadService;
import ru.nsofronov.datafeedtest.database.model.Fixture;

import java.util.List;

/**
 * Сервис, загружающий данные только через нашу базу данных
 *
 * @author nsofronov
 */
@Service("databaseDataLoadService")
public class DatabaseDataLoadService implements DataLoadService {

	@Autowired
	private DatabaseDAO databaseDAO;

	/**
	 * Загрузкам данных
	 *
	 * @param request запрос
	 * @return результат загрузки
	 */
	@Override
	public DataLoadResponse load(DataLoadRequest request) {
		DataLoadResponse response = new DataLoadResponse();
		Integer yearFrom = request.getYearFrom();
		Integer yearTo = request.getYearTo();
		String competitionName = request.getCompetitionName();
		String teamName = request.getTeamName();
		List<Fixture> list = databaseDAO.findFixtures(yearFrom, yearTo, competitionName, teamName);
		response.setFixtures(list);
		return response;
	}
}
