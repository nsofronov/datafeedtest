package ru.nsofronov.datafeedtest.database.service;

import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;

/**
 * Интерфейс логирования запросов пользователя
 *
 * @author nsofronov
 */
public interface LogService {
	/**
	 * Выполнение логирования
	 *
	 * @param request запрос
	 * @param response ответ
	 */
	void log(DataLoadRequest request, DataLoadResponse response);
}
