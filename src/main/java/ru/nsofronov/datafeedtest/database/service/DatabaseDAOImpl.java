package ru.nsofronov.datafeedtest.database.service;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;
import ru.nsofronov.datafeedtest.database.model.Fixture;
import ru.nsofronov.datafeedtest.database.model.Identifiable;
import ru.nsofronov.datafeedtest.database.model.RequestLog;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

import static ru.nsofronov.datafeedtest.database.model.Fixture.FIND_FIXTURES_QUERY;

/**
 * Репозиторий для работы с базой данных
 *
 * @author nsofronov
 */
@Repository
@Transactional
public class DatabaseDAOImpl implements DatabaseDAO {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Запрос матчей, хранящихся в базе
	 *
	 * @param yearFrom год от
	 * @param yearTo год до
	 * @param competitionName название турнира
	 * @param teamName название команды
	 * @return матчи
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Fixture> findFixtures(Integer yearFrom, Integer yearTo, String competitionName, String teamName) {
		TypedQuery<Fixture> typedQuery = em.createNamedQuery(FIND_FIXTURES_QUERY, Fixture.class);
		typedQuery.setParameter("yearFrom", yearFrom);
		typedQuery.setParameter("yearTo", yearTo);
		typedQuery.setParameter("competitionName", competitionName);
		typedQuery.setParameter("teamName", teamName);
		return typedQuery.getResultList();
	}

	/**
	 * Загрузка сущностей в базу
	 * Сперва проверяет, есть ли уже эта сущность была создана, то только обновит информацию
	 *
	 * @param response - набор загружаемых сущностей
	 */
	@Override
	@Transactional(readOnly = false)
	public void merge(DataLoadResponse response) {
		merge(response.getTeam());
		response.getCompetitions().forEach(this::merge);
		response.getFixtures().forEach(this::merge);
		em.flush();
	}

	/**
	 * Логирование пользовательских запросов
	 *
	 * @param request запрос
	 * @param response ответ
	 */
	@Override
	public void createLog(DataLoadRequest request, DataLoadResponse response) {
		RequestLog requestLog = new RequestLog();

		requestLog.setDate(new Date());
		requestLog.setDataLoadType(request.getLoadType());
		requestLog.setDataExportType(request.getExportType());
		requestLog.setCompetitionName(request.getCompetitionName());
		requestLog.setTeamName(request.getTeamName());
		requestLog.setYearFrom(request.getYearFrom());
		requestLog.setYearFrom(request.getYearTo());
		requestLog.setFileName(response.getName(request));
		requestLog.setResultCount(response.getFixtures().size());

		em.persist(requestLog);
		em.flush();
	}

	/**
	 * Загрузка сущностей в базу
	 * Сперва проверяет, есть ли уже эта сущность была создана, то только обновит информацию
	 *
	 * @param r сущность
	 * @param <T> тип сущности
	 */
	private <T extends Identifiable> void merge(T r) {
		if (r instanceof Fixture) {
			Fixture fixture = (Fixture) r;
			fixture.setCompetition(em.find(fixture.getCompetition().getClass(), fixture.getCompetition().getId()));
			fixture.setHomeTeam(em.find(fixture.getHomeTeam().getClass(), fixture.getHomeTeam().getId()));
			fixture.setAwayTeam(em.find(fixture.getAwayTeam().getClass(), fixture.getAwayTeam().getId()));
		}
		Identifiable competition = em.find(r.getClass(), r.getId());
		if (competition == null) {
			em.persist(r);
		} else {
			em.merge(r);
		}
	}
}
