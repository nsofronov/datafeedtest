package ru.nsofronov.datafeedtest.database.model;

import java.io.Serializable;

/**
 * Общий интерфейс для сущнотей
 *
 * @author nsofronov
 */
public interface Identifiable extends Serializable {
	/**
	 * Получение id сущности
	 * @return id
	 */
	public Long getId();
}
