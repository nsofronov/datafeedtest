package ru.nsofronov.datafeedtest.database.model;

import java.util.stream.Stream;

/**
 * Статус матча
 *
 * @author n.sofronov
 */
public enum FixtureStatus {
	SCHEDULED("SCHEDULED"),
	CANCELED("CANCELED"),
	TIMED("TIMED"),
	IN_PLAY("IN_PLAY"),
	POSTPONED("POSTPONED"),
	FINISHED("FINISHED");

	private String name;

	FixtureStatus(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static FixtureStatus getStatus(String status) {
		return Stream.of(values()).filter(s -> s.getName().equals(status)).findFirst().orElse(null);
	}
}
