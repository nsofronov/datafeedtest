package ru.nsofronov.datafeedtest.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

import static ru.nsofronov.datafeedtest.database.model.Fixture.FIND_FIXTURES_QUERY;

/**
 * Матч
 *
 * @author n.sofronov
 */
@Entity
@Table(schema = "FOOTBALL", name = "FIXTURE")
@NamedQueries({ @NamedQuery(name = "Fixture.findAllFixtures", query = "from ru.nsofronov.datafeedtest.database.model.Fixture"),
					  @NamedQuery(name = FIND_FIXTURES_QUERY, query = "select fixture from Fixture as fixture "
							  + "join fixture.competition as competition join fixture.homeTeam as homeTeam "
							  + "join fixture.awayTeam as awayTeam where competition.year between :yearFrom and :yearTo "
							  + "and (lower(competition.caption) = :competitionName or lower(competition.league) = :competitionName) "
							  + "and (lower(homeTeam.name) = :teamName or lower(homeTeam.code) = :teamName "
							  + "or lower(homeTeam.shortName) = :teamName or lower(awayTeam.name) = :teamName "
							  + "or lower(awayTeam.code) = :teamName or lower(awayTeam.shortName) = :teamName)") })
public class Fixture implements Identifiable {
	public static final String FIND_FIXTURES_QUERY = "Fixture.findFixtures";
	@Id
	@Column(name = "FIXTURE_ID")
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "COMPETITION_ID")
	private Competition competition;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "HOME_TEAM_ID")
	private Team homeTeam;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "AWAY_TEAM_ID")
	private Team awayTeam;

	@Column(name = "FIXTURE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@Column(name = "FIXTURE_STATUS")
	@Enumerated(EnumType.STRING)
	private FixtureStatus status;

	@Column(name = "GOALS_HOME_TEAM")
	private Integer goalsHomeTeam;

	@Column(name = "GOALS_AWAY_TEAM")
	private Integer goalsAwayTeam;

	@Column(name = "IS_PENALTY")
	private Boolean isPenalty;

	@Column(name = "PENALTY_GOALS_HOME_TEAM")
	private Integer penaltyGoalsHomeTeam;

	@Column(name = "PENALTY_GOALS_AWAY_TEAM")
	private Integer penaltyGoalsAwayTeam;

	protected Fixture() {
	}

	public Fixture(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Competition getCompetition() {
		return competition;
	}

	public void setCompetition(Competition competition) {
		this.competition = competition;
	}

	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Team getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public FixtureStatus getStatus() {
		return status;
	}

	public void setStatus(FixtureStatus status) {
		this.status = status;
	}

	public Integer getGoalsHomeTeam() {
		return goalsHomeTeam;
	}

	public void setGoalsHomeTeam(Integer goalsHomeTeam) {
		this.goalsHomeTeam = goalsHomeTeam;
	}

	public Integer getGoalsAwayTeam() {
		return goalsAwayTeam;
	}

	public void setGoalsAwayTeam(Integer goalsAwayTeam) {
		this.goalsAwayTeam = goalsAwayTeam;
	}

	public Boolean getPenalty() {
		return isPenalty;
	}

	public void setPenalty(Boolean penalty) {
		isPenalty = penalty;
	}

	public Integer getPenaltyGoalsHomeTeam() {
		return penaltyGoalsHomeTeam;
	}

	public void setPenaltyGoalsHomeTeam(Integer penaltyGoalsHomeTeam) {
		this.penaltyGoalsHomeTeam = penaltyGoalsHomeTeam;
	}

	public Integer getPenaltyGoalsAwayTeam() {
		return penaltyGoalsAwayTeam;
	}

	public void setPenaltyGoalsAwayTeam(Integer penaltyGoalsAwayTeam) {
		this.penaltyGoalsAwayTeam = penaltyGoalsAwayTeam;
	}
}
