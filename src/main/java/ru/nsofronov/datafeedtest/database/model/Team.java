package ru.nsofronov.datafeedtest.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Команда
 *
 * @author n.sofronov
 */
@Entity
@Table(schema = "FOOTBALL", name = "TEAM")
@NamedQueries({ @NamedQuery(name = "Team.findAllTeams", query = "from ru.nsofronov.datafeedtest.database.model.Team") })
public class Team implements Identifiable {
	@Id
	@Column(name = "TEAM_ID")
	private Long id;

	@Column(name = "TEAM_NAME")
	private String name;

	@Column(name = "TEAM_CODE")
	private String code;

	@Column(name = "TEAM_SHORT_NAME")
	private String shortName;

	protected Team() {
	}

	public Team(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
}
