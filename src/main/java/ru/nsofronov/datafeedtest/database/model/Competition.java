package ru.nsofronov.datafeedtest.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Чемпионат
 *
 * @author n.sofronov
 */
@Entity
@Table(schema = "FOOTBALL", name = "COMPETITION")
@NamedQueries({ @NamedQuery(name = "Competition.findAllCompetitions", query = "from ru.nsofronov.datafeedtest.database.model.Competition") })
public class Competition implements Identifiable {
	@Id
	@Column(name = "COMPETITION_ID")
	private Long id;

	@Column(name = "CAPTION")
	private String caption;

	@Column(name = "LEAGUE")
	private String league;

	@Column(name = "YEAR")
	private Integer year;

	protected Competition() {
	}

	public Competition(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
}
