package ru.nsofronov.datafeedtest.database.model;

import ru.nsofronov.datafeedtest.api.DataExportType;
import ru.nsofronov.datafeedtest.api.DataLoadType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Событие запроса клиента
 *
 * @author nsofronov
 */
@Entity
@Table(schema = "FOOTBALL", name = "REQUEST_LOG")
@NamedQueries({ @NamedQuery(name = "RequestLog.findAllLogs", query = "from ru.nsofronov.datafeedtest.database.model.RequestLog") })
public class RequestLog {
	@Id
	@Column(name = "REQUEST_LOG_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "log_sequence")
	@SequenceGenerator(name = "log_sequence", sequenceName = "log_sequence")
	private Long id;

	@Column(name = "REQUEST_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@Column(name = "DATA_LOAD_TYPE")
	@Enumerated(EnumType.STRING)
	private DataLoadType dataLoadType;

	@Column(name = "DATA_EXPORT_TYPE")
	@Enumerated(EnumType.STRING)
	private DataExportType dataExportType;

	@Column(name = "COMPETITION_NAME")
	private String competitionName;

	@Column(name = "TEAM_NAME")
	private String teamName;

	@Column(name = "YEAR_FROM")
	private Integer yearFrom;

	@Column(name = "YEAR_TO")
	private Integer yearTo;

	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "RESULT_COUNT")
	private Integer resultCount;

	public RequestLog() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public DataLoadType getDataLoadType() {
		return dataLoadType;
	}

	public void setDataLoadType(DataLoadType dataLoadType) {
		this.dataLoadType = dataLoadType;
	}

	public DataExportType getDataExportType() {
		return dataExportType;
	}

	public void setDataExportType(DataExportType dataExportType) {
		this.dataExportType = dataExportType;
	}

	public String getCompetitionName() {
		return competitionName;
	}

	public void setCompetitionName(String competitionName) {
		this.competitionName = competitionName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Integer getYearFrom() {
		return yearFrom;
	}

	public void setYearFrom(Integer yearFrom) {
		this.yearFrom = yearFrom;
	}

	public Integer getYearTo() {
		return yearTo;
	}

	public void setYearTo(Integer yearTo) {
		this.yearTo = yearTo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getResultCount() {
		return resultCount;
	}

	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}
}
