package ru.nsofronov.datafeedtest.integration.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.nsofronov.datafeedtest.api.DataExportType;
import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;
import ru.nsofronov.datafeedtest.api.DataLoadType;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author nsofronov
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:config/DataSource.xml")
public class IntegrationDataLoadServiceTest {

	@Spy
	@Autowired
	@Qualifier("integrationDataLoadService")
	private IntegrationDataLoadService loadService;

	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(loadService.executeRequest(anyObject(), anyObject())).thenReturn(null);
	}

	@Test
	public void readCompetitionsTest() {
		DataLoadRequest request = new DataLoadRequest();
		request.setCompetitionName("123");
		request.setExportType(DataExportType.CSV);
		request.setLoadType(DataLoadType.FULL);
		request.setTeamName("234");
		request.setYearFrom(2016);
		request.setYearTo(2017);
		DataLoadResponse response = loadService.load(request);
		assertTrue(response.getFixtures().isEmpty());
	}
}
