package ru.nsofronov.datafeedtest.database.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;
import ru.nsofronov.datafeedtest.database.model.Competition;
import ru.nsofronov.datafeedtest.database.model.Fixture;
import ru.nsofronov.datafeedtest.database.model.RequestLog;
import ru.nsofronov.datafeedtest.database.model.Team;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author n.sofronov
 */
@ContextConfiguration(locations = "classpath:config/DataSource.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class DatabaseDAOTest extends Assert {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private DatabaseDAO databaseDAO;

	/**
	 * Очищаем таблицу
	 */
	@Before
	@Transactional(readOnly = false)
	public void clearTables() {
		List<Fixture> requestLogs = em.createNamedQuery("Fixture.findAllFixtures", Fixture.class).getResultList();
		List<Competition> competitions = em.createNamedQuery("Competition.findAllCompetitions", Competition.class).getResultList();
		List<Team> teams = em.createNamedQuery("Team.findAllTeams", Team.class).getResultList();
		requestLogs.forEach(em::remove);
		competitions.forEach(em::remove);
		teams.forEach(em::remove);
		em.flush();
	}

	@Test
	@Transactional(readOnly = false)
	public void mergeTest() {
		DataLoadResponse response = new DataLoadResponse();

		Competition competition = new Competition(1L);
		competition.setLeague("123");
		competition.setYear(2016);
		response.getCompetitions().add(competition);

		Team team = new Team(1L);
		team.setCode("123");
		team.setShortName("234");
		response.setTeam(team);

		Fixture fixture = new Fixture(1L);
		fixture.setCompetition(competition);
		fixture.setHomeTeam(team);
		fixture.setAwayTeam(team);
		response.getFixtures().add(fixture);

		databaseDAO.merge(response);

		assertNotNull(em.find(Competition.class, 1L));
		assertNotNull(em.find(Team.class, 1L));
		assertNotNull(em.find(Fixture.class, 1L));
	}
}
