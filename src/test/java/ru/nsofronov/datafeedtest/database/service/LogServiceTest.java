package ru.nsofronov.datafeedtest.database.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.nsofronov.datafeedtest.api.DataExportType;
import ru.nsofronov.datafeedtest.api.DataLoadRequest;
import ru.nsofronov.datafeedtest.api.DataLoadResponse;
import ru.nsofronov.datafeedtest.api.DataLoadType;
import ru.nsofronov.datafeedtest.database.model.RequestLog;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author n.sofronov
 */
@ContextConfiguration(locations = "classpath:config/DataSource.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class LogServiceTest extends Assert {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private LogService logService;

	/**
	 * Очищаем таблицу с историе о запросах
	 */
	@Before
	@Transactional(readOnly = false)
	public void clearTables() {
		List<RequestLog> requestLogs = em.createNamedQuery("RequestLog.findAllLogs", RequestLog.class).getResultList();
		requestLogs.forEach(em::remove);
		em.flush();
	}

	/**
	 * Очищаем таблицу с историе о запросах
	 */
	@Test
	@Transactional(readOnly = false)
	public void createLogTest() {
		DataLoadRequest request = new DataLoadRequest();
		request.setCompetitionName("123");
		request.setExportType(DataExportType.CSV);
		request.setLoadType(DataLoadType.FULL);
		request.setTeamName("234");
		request.setYearFrom(2016);
		request.setYearTo(2017);
		DataLoadResponse response = new DataLoadResponse();
		logService.log(request, response);

		assertTrue(em.createNamedQuery("RequestLog.findAllLogs", RequestLog.class).getResultList().size() == 1);
	}
}
