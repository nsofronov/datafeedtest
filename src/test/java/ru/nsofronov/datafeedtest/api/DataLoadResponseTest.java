package ru.nsofronov.datafeedtest.api;

import org.junit.Assert;
import org.junit.Test;
import ru.nsofronov.datafeedtest.database.model.Competition;
import ru.nsofronov.datafeedtest.database.model.Team;

import java.util.stream.Stream;

/**
 * @author n.sofronov
 */
public class DataLoadResponseTest extends Assert {

	/**
	 * Тест формирования имени файла для экпорта
	 */
	@Test
	public void getNameTest() {
		DataLoadRequest request = new DataLoadRequest();
		request.setTeamName("KK");
		request.setCompetitionName("CC");
		request.setYearFrom(2016);
		request.setYearTo(2017);
		request.setExportType(DataExportType.XML);
		DataLoadResponse response = new DataLoadResponse();
		assertEquals("KKCC20162017.xml", response.getName(request));

		Competition competition = new Competition(0L);
		competition.setLeague("CCC");
		response.getCompetitions().add(competition);
		assertEquals("KKCCC20162017.xml", response.getName(request));

		competition = new Competition(0L);
		competition.setLeague("CCC");
		response.getCompetitions().add(competition);
		assertEquals("KKCCC20162017.xml", response.getName(request));

		competition = new Competition(0L);
		competition.setLeague("CCCC");
		response.getCompetitions().add(competition);
		assertEquals("KKCC20162017.xml", response.getName(request));

		Team team = new Team(0L);
		team.setShortName("KKK");
		response.setTeam(team);
		assertEquals("KKKCC20162017.xml", response.getName(request));
	}
}
